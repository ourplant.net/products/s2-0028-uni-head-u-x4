Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/01_operating_manual/S2-0006_F_BA_Uni%20Head.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/01_operating_manual/S2-0006_D_OM_Uni%20Head.pdf) |
|assembly drawing           |[de/en](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/02_assembly_drawing/s2-0028_B_ZNB_uni-head_U-X4.pdf)|
|circuit diagram            |[de/en](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/03_circuit_diagram/S2-0028_UNI-HEAD_U-X4.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/04_maintenance_instructions/S2-0006_B_WA_Uni%20Head.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/04_maintenance_instructions/S2-0006_A_MI_Uni%20Head.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/05_spare_parts/S2-0006_C_EVL_Uni%20Head.pdf) / [en](https://gitlab.com/ourplant.net/products/s2-0028-uni-head-u-x4/-/raw/main/05_spare_parts/S2-0006_C_SWP_Uni%20Head.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
